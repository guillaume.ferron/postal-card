
-- -----------------------------------------------------
-- Database Postal Card
-- -----------------------------------------------------
CREATE DATABASE IF NOT EXISTS postalcard;
use postalcard

-- -----------------------------------------------------
-- User manager
-- -----------------------------------------------------
GRANT ALL PRIVILEGES ON postalcard.* TO 'manager'@'localhost' IDENTIFIED BY 'manager';


-- -----------------------------------------------------
-- Table des utilisateurs du service
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS utilisateurs (
	  id INTEGER NOT NULL AUTO_INCREMENT,
	  nom VARCHAR(255) NOT NULL,
	  prenom VARCHAR(255) NOT NULL,
	  email VARCHAR(255) NOT NULL,
	  PRIMARY KEY (id));
