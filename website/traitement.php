<?php

/*
 * Postal Card Maker (MIMO 2018-2019)
 * */


//Configuration
$config = array(
	"image_dir" => "images",
	"cartes_dir" => "cartes",
	"email" => "",
	"password" => "",
	"message" => "Vous trouverez ci-joint votre carte postale. Merci d'avoir utilisé notre service, nous espérons vous revoir très rapidement pour toujours plus de cartes postales!",
	"text-options" => array(
		"fontpath" => "fonts/Arial.ttf",
		"fontcolor" => array(
			"red" => 0,
			"green" => 0,
			"blue" => 0
		)
	)
);


//On importe les dépendances nécessaires
require_once('class/db.php');
require_once('class/maker.php');
require_once('class/mail.php');
require_once(__DIR__ . '/class/PHPMailer/src/PHPMailer.php');


//On réceptionne les données utilisateurs
$image = $_FILES['image'];
$data = array(
	"nom" => $_POST['nom'],
	"prenom" => $_POST['prenom'],
	"email" => $_POST['email'],
	"sendme" => isset($_POST['sendme']),
	"message" => $_POST['message']
);

//On sécurise un minimum les données
$secure_data = array();
foreach ($data as $key => $value) {
	$encoded_value = htmlentities($value, ENT_QUOTES);
	$secure_value = filter_var($encoded_value, FILTER_SANITIZE_STRING);
	$secure_data[$key] = $secure_value;
	unset($encoded_value);
	unset($secure_value);
}
unset($data);

//On stocke les données valorisables dans une base de données;
$db = new Db;
$db->query('INSERT INTO utilisateurs (nom, prenom, email) VALUES ("'.$secure_data['nom'].'","'.$secure_data['prenom'].'","'.$secure_data['email'].'");');

//On stocke l'id utilisateur, plus récent id entré dans la base, donc le chiffre le plus élevé
$res = $db->query('SELECT id FROM utilisateurs ORDER BY id DESC;');
$id = $res[0];
unset($res);

//#FIX On stocke l'image sur le disque dur (fixe un effet de bord lié à la volatilité des fichiers temporaires coté OS)
move_uploaded_file($image['tmp_name'], $config['image_dir'].'/'.$image['name']);
$image['path'] = $config['image_dir'].'/'.$image['name'];//ICI

//On fabrique la carte postale
$maker = new Maker;
$options = $config['text-options'];

$path = $maker->make($image, $secure_data['message'], $options);

	//echo $path;


unset($options);
unset($maker);

//On récupére les données membres
$res = $db->query('SELECT * FROM utilisateurs WHERE id="'.$id['id'].'";');
$data = $res[0];
unset($res);

//On envoit l'image à l'utilisateur si il a coché la case
if($secure_data['sendme']) {
	$mail = new Mail;
	$errors = $mail->send($path,$data['email'],$data['nom'],$data['prenom'],$config);
	//On prévient l'utilisateur si sa carte  a pu ou pas être envoyé
	if(!$errors) echo 'Une copie de cette image a été envoyée à votre adresse mail :)';
	else var_dump($errors);
}

//On affiche la carte
echo '<img style="width:100%;" src="'.$path.'"/>';

unset($secure_data);
unset($errors);
unset($path);
unset($data);
unset($config);
unset($mail);

