<?php


class Maker {

	function __construct() {}

	function make(array $image, string $uri, array $options) {

		//On récupère le path de l'image sur laquelle on veut afficher le message
		$url = $image['path'];

		//On enregistre son extension
		$ext = pathinfo($image['name'], PATHINFO_EXTENSION);

		//On décide, à partir de l'extension, quel traitement on veut lui associer
		switch($ext) {
		case 'png':
			$image = imagecreatefrompng($url);
			break;

		case 'jpg':
		case 'jpeg':
			$image = imagecreatefromjpeg($url);
			break;

		}

		//On met l'image à une echelle de référence #FIX : corrige un bug d'affichage lorsque les images sont trop différentes
		$image = imagescale($image , 1200);

		//On enregistre les dimensions de l'image
		$width = imagesx($image);
		$height = imagesy($image);


		// On enregistre la police choisit par l'utilisateur si elle existe, sinon on met une police ARIAL par default
		if(file_exists ($options['fontpath'])) {
			$font_path = __DIR__ . '/../' . trim($options['fontpath'], '/');
		} else {
			$font_path = __DIR__ . '/../fonts/Arial.ttf';
		}

		// On enregistre la couleur de texte choisit par l'utilisateur si elle existe, sinon on met une couleur par default
		if($options['fontcolor']) {
			$color = imagecolorallocate($image, 
				$options['fontcolor']['red'], 
				$options['fontcolor']['green'], 
				$options['fontcolor']['blue']);      
		} else {
			$color = imagecolorallocate($image, 0, 0, 0);      
		}

		//On insère une surcouche (calque) transparente pour être sur de pouvoir bien lire le texte quoiqu'il arrive
		$bcolor=imagecolorallocatealpha($image , 0xFF, 0xFF, 0xFF, 310);
		imagefilledrectangle($image,  0,$height-200, $width, $height, $bcolor);

		//On calcule la répartition du texte dans l'image, on essaye de couper aux espaces pour ne pas rompre la logique des phrases
		$line=140;
		$c1=0;
		$tours=strlen($uri)/$line;
		for($i=$tours; $i>0; $i--) {
			$c2=$line;
			$text = substr($uri,$c1,$line);
			while((substr($text, -1) != ' ')) {
				if($i>1) {
					$text = substr($uri,$c1,$line--);
				} else {
					$text=" ";
				}
			}
			imagettftext ($image, 14, 0, 25, $height-50-($i*20), $color , $font_path , substr($uri,$c1,$line));
			$c1=$c1+$line;
			$line=$c2;
		}

		// On enregistre l'image
		$cart_path = 'cartes/' . hash_file('md5', $url) . '.' . $ext;
		switch($ext) {
		case 'png':
			imagepng($image, $cart_path);
			break;

		case 'jpg':
		case 'jpeg':
			imagejpeg($image, $cart_path);
			break;

		}

		// On nettoie l'image en mémoire (conseillé par la doc)
		imagedestroy($image);

		//On retourne le path de l'image modifiée
		return $cart_path;


	}

}
