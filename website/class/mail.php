<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require __DIR__ . '/PHPMailer/src/Exception.php';
require __DIR__ . '/PHPMailer/src/PHPMailer.php';
require __DIR__ . '/PHPMailer/src/SMTP.php';

class Mail {

	private $mail;

	function __construct() {

		//On enregistre dans la "mémoire de la classe" (en attribut privé, accessible uniquement par elle meme) 
		//l'instance de PHPmailer
		$mail = new PHPMailer();
		$this->mail = $mail;
	}

	function send($path,$email,$nom,$prenom,$config) {

		//On récupère l'extension du fichier car on en aura besoin plus tard
		$ext = pathinfo(basename($path), PATHINFO_EXTENSION);

		//On configure l'instance de PHPmailer comme la doc le demande
		$this->mail->IsSMTP();
		$this->mail->SMTPDebug = 0; // mettre 1 pour le debug serveur, 2 pour le debug client/serveur 
		$this->mail->SMTPAuth = true;
		$this->mail->SMTPSecure = 'ssl';
		$this->mail->Host = "smtp.gmail.com";
		$this->mail->Port = 465;
		$this->mail->IsHTML(true);
		$this->mail->Username = $config['email'];
		$this->mail->Password = $config['password'];
		$this->mail->SetFrom($config['email'], 'La Carte Postal');
		$this->mail->AddReplyTo($config['email'], 'La Carte Postal');
		$this->mail->Subject = "Votre Carte Postal!";
		$this->mail->Body = "Bonjour ".$prenom." ".$nom."<br>".$config['message'];
		$this->mail->AddAddress($email);
		$this->mail->addAttachment($path);
		$this->mail->AltBody = "Pour voir ce message utilisez un navigateur plus récent!";

		//On envoit!
		if(!$this->mail->Send()) {
			return "Mailer Error: " . $this->mail->ErrorInfo;
		}
	}

}
