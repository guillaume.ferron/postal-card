<?php


//TODO : faire des fonctions plus spécifiques d'enregistrement base. Afin par exmple de ne pas créer une entrée par personne utilisant le service mais plutôt d'incrémenter une valeur numérique dans une nouvelle colonne intitulée "utilisation du service". A voir dans une V2.

class Db {

	private $pdo;

	function __construct() {

		$host = 'localhost';
		$db   = 'postalcard';
		$user = 'manager';
		$pass = 'manager';
		$charset = 'utf8mb4';

		$dsn = "mysql:host=$host;dbname=$db;charset=$charset";
		$options = [
			PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
			PDO::ATTR_EMULATE_PREPARES   => false,
		];
		try {
			$pdo = new PDO($dsn, $user, $pass, $options);
			$this->pdo = $pdo;
		} catch (\PDOException $e) {
			throw new \PDOException($e->getMessage(), (int)$e->getCode());
		}
	}

	function query(string $query) {

		$result = $this->pdo->prepare($query); 
		$result->execute(); 
		return $result->fetchAll(PDO::FETCH_BOTH); 
	}

}
